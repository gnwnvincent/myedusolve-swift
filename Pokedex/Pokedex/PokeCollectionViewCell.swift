//
//  PokeCollectionViewCell.swift
//  Pokedex
//
//  Created by Vincent Gunawan on 13/11/22.
//

import UIKit

class PokeCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var pokeNameLabelView: UILabel!
  
  override class func awakeFromNib() {
    
  }
}
