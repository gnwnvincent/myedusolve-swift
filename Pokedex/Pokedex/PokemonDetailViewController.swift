//
//  PokemonDetailViewController.swift
//  Pokedex
//
//  Created by Vincent Gunawan on 13/11/22.
//

import UIKit
import Alamofire
import SafariServices

class PokemonDetailViewController: UIViewController {
  @IBOutlet weak var elementLabel: UILabel!
  
  @IBOutlet weak var buttonView: UIButton!
  @IBOutlet weak var pokeID: UILabel!
  @IBOutlet weak var defenseLabel: UILabel!
  @IBOutlet weak var pokemonImageView: UIImageView!
  @IBOutlet weak var pokemonLabelView: UILabel!
  
  @IBOutlet weak var weightLabel: UILabel!
  @IBOutlet weak var heightLabel: UILabel!

  @IBOutlet weak var attackLabel: UILabel!
  var pokemon: Pokemon?
  var pokeDetail: PokemonDetail?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.fetchPokemonDetail()
    self.setImageProperties()
    self.title = self.pokemon?.name?.capitalized
    self.pokemonLabelView.text = self.pokemon?.name?.capitalized
    self.pokemonImageView.sd_setImage(with: URL(string: self.pokemon?.imageUrl ?? ""))
    self.buttonView.setTitle("More Info", for: .normal)
  }
  
  @IBAction func didTapMoreInfo(_ sender: Any) {
    let url = URL(string: "https://pokemondb.net/pokedex/" + (pokemon?.name ?? ""))!
    let config = SFSafariViewController.Configuration()
    config.entersReaderIfAvailable = true

    let vc = SFSafariViewController(url: url, configuration: config)
      present(vc, animated: true)
  }
  
  func setImageProperties() {
    self.pokemonImageView.layer.cornerRadius = 8
    self.pokemonImageView.backgroundColor = .label
  }
  
  func fetchPokemonDetail(){
    let urlRequest: Alamofire.URLRequestConvertible = URLRequest(url: URL(string: pokemon?.url ?? "")!)
    AF.request(urlRequest).responseDecodable(of: PokemonDetail.self) { response in
      switch response.result{
      case .success(let result):
        
        self.elementLabel.text = self.mapTypestoElement(types: result.types ?? []).joined(separator: "\n")
        self.defenseLabel.text = String(result.stats?[2].baseStat ?? 0)
        self.heightLabel.text = String(result.height ?? 0)
        self.pokeID.text = String(result.id ?? 0)
        self.weightLabel.text = String(result.weight ?? 0)
        self.attackLabel.text = String(result.stats?[1].baseStat ?? 0)
      case .failure(_):
        break
      }
    }
  }
  
  func mapTypestoElement(types: [TypeElement]) -> [String] {
    types.map { type in
      (type.type?.name ?? "unknown").capitalized
    }
  }
}
