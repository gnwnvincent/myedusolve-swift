//
//  ViewController.swift
//  Pokedex
//
//  Created by Vincent Gunawan on 13/11/22.
//

import UIKit
import Alamofire
import SDWebImage

class ViewController: UIViewController{
  
  var pokemons: [Pokemon]? {
    didSet {
      self.collectionView.reloadData()
    }
  }
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.fetchPokemons()
  }
  
  func fetchPokemons() {
    AF.request("https://pokeapi.co/api/v2/pokemon/").responseDecodable(of: Pokemons.self) { response in
      switch response.result{
      case .success(let result):
        self.pokemons = result.pokemons
      case .failure(_):
        break
      }
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    switch segue.identifier {
    case "showPokeDetail":
      let vc = segue.destination as? PokemonDetailViewController
      if let pokemon = sender as? Pokemon {
        vc?.pokemon = pokemon
      }
    default:
      break
    }
  }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.pokemons?.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokeCollectionViewCell", for: indexPath) as? PokeCollectionViewCell else {
      return UICollectionViewCell(frame: .zero)
    }
    
    if let pokemon = self.pokemons?[indexPath.row] {
      cell.pokeNameLabelView.text = pokemon.name?.capitalized
      cell.imageView.sd_setImage(with: URL(string: pokemon.imageUrl))
      
    }
    cell.layer.cornerRadius = 8
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let side = collectionView.frame.width/3.0 - 16.0
    
    return CGSize(width: side, height: side)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: .zero, left: 8.0, bottom: .zero, right: 8.0)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
      return 8
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return 8
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if let pokemon = self.pokemons?[indexPath.item] {
      performSegue(withIdentifier: "showPokeDetail", sender: pokemon)
    }
  }
}
