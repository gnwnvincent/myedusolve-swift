//
//  ViewController.swift
//  SimpleDiscountCalculator
//
//  Created by Vincent Gunawan on 11/10/22.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet var discountFieldView: UITextField!
  @IBOutlet var discountView: UILabel!
  @IBOutlet var priceView: UILabel!
  @IBOutlet var priceFieldView: UITextField!
  @IBOutlet var resetView: UIButton!
  @IBOutlet var savingView: UILabel!
  @IBOutlet var finalView: UILabel!
  @IBOutlet var savingPriceView: UILabel!
  @IBOutlet var finalPriceValueView: UILabel!
  @IBOutlet var buttonView: UIButton!
  @IBOutlet var titleView: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setLabel()
  }
  
  func setLabel() {
    self.titleView.text = "Simple Discount Calculator"
    self.priceView.text = "Original Price"
    self.discountView.text = "Discount (%)"
    self.savingView.text = "You save:"
    self.finalView.text = "Final price:"
    self.savingPriceView.text = "0.0"
    self.finalPriceValueView.text = "0.0"
    self.buttonView.setTitle("Calculate", for: .normal)
    self.resetView.setTitle("Reset", for: .normal)
  }
  
  func calculatePrice(price: Int, discount: Int) -> (Double, Double) {
    return (Double(price * discount)/100, Double(price * (100 - discount))/100)
  }

  @IBAction func calculate(_ sender: Any) {
    let price = Int(priceFieldView.text ?? "") ?? 0
    let discount = Int(discountFieldView.text ?? "") ?? 0
    let (discountPrice, finalPrice) = calculatePrice(price: price, discount: discount)
    self.savingPriceView.text = String(discountPrice)
    self.finalPriceValueView.text = String(finalPrice)
  }
  
  @IBAction func reset(_ sender: Any) {
    self.discountFieldView.text = ""
    self.priceFieldView.text = ""
    self.savingPriceView.text = "0.0"
    self.finalPriceValueView.text = "0.0"
  }
}

