//
//  ViewController.swift
//  UIKitDay1
//
//  Created by Vincent Gunawan on 10/10/22.
//

import UIKit

class ViewController: UIViewController {

  let imageDark = UIImage(named: "imageDark")!
  let imageLight = UIImage(named: "imageLight")!
  var darkToogle: Bool = false
  @IBOutlet var nameView: UILabel!
  @IBOutlet var subtitleView: UILabel!
  @IBOutlet var buttonView: UIButton!
  @IBOutlet var imageView: UIImageView!
  override func viewDidLoad() {
    super.viewDidLoad()
  
    if self.traitCollection.userInterfaceStyle == .dark {
      darkToogle = true
      self.imageView.image = imageDark
      
    } else {
      darkToogle = false
      self.imageView.image = imageLight
    }
    
    self.nameView.text = "Vincent Gunawan"
    self.subtitleView.text = "QA Intern @Stockbit"
    self.buttonView.setTitle("Change State", for: .normal)
  }

  @IBAction func changeState(_ sender: Any) {
    darkToogle.toggle()
    if darkToogle {
      self.view.backgroundColor = .black
      self.nameView.textColor = .white
      self.subtitleView.textColor = .white
      self.imageView.image = imageDark
    } else {
      self.view.backgroundColor = .white
      self.nameView.textColor = .black
      self.subtitleView.textColor = .black
      self.imageView.image = imageLight
    }
  }
  
}

