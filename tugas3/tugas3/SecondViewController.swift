//
//  SecondViewController.swift
//  tugas3
//
//  Created by Vincent Gunawan on 23/10/22.
//

import UIKit

class SecondViewController: UIViewController {
  
  var colorObject: UIColor?
  var colorName: String?
  
  @IBOutlet var showThirdButton: UIButton!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    showThirdButton.tintColor = colorObject
    print(colorName ?? "undefined color")
  }
  
  @IBAction func showThirdTapped(_ sender: Any) {
    let colorName: String = self.colorName ?? ""
    let colorObject: UIColor = self.colorObject ?? UIColor.systemFill
    print(colorName)
    print(colorObject)
    let sender: [String: Any] = ["colorName": colorName, "colorObject": colorObject]
    performSegue(withIdentifier: "showThird", sender: sender)
  }
  
  @IBAction func backTapped(_ sender: Any) {
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showThird" {
      let thirdView = segue.destination as! ThirdViewController
      let object = sender as! [String: Any]
      thirdView.colorObject = object["colorObject"] as? UIColor
      thirdView.colorName = (object["colorName"] as? String)!
    }
  }
  
}
