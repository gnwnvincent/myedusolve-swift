//
//  ThirdViewController.swift
//  tugas3
//
//  Created by Vincent Gunawan on 23/10/22.
//

import UIKit

class ThirdViewController: UIViewController {
  
  var colorName: String?
  var colorObject: UIColor?
  
  @IBOutlet var labelView: UILabel!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    labelView.text = "The color name is: \(colorName ?? "undefined")"
    labelView.backgroundColor = colorObject ?? UIColor.systemFill
  }
  
  @IBAction func backButton(_ sender: Any) {
    self.navigationController?.popToRootViewController(animated: true)
  }
  
}
