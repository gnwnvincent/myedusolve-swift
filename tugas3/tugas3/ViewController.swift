//
//  ViewController.swift
//  tugas3
//
//  Created by Vincent Gunawan on 23/10/22.
//

import UIKit

class ViewController: UIViewController {
  
  @IBOutlet var greenButton: UIButton!
  @IBOutlet var blueButton: UIButton!
  @IBOutlet var redButton: UIButton!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    var config = UIButton.Configuration.filled()
    config.baseBackgroundColor = .red
    redButton.configuration = config
    config.baseBackgroundColor = .blue
    blueButton.configuration = config
    config.baseBackgroundColor = .green
    greenButton.configuration = config
  }
  
  @IBAction func redTapped(_ sender: Any) {
    let sender: [String: Any] = ["colorName": "red", "colorObject": UIColor.systemRed]
    performSegue(withIdentifier: "showSecond", sender: sender)
  }
  
  @IBAction func blueTapped(_ sender: Any) {
    let sender: [String: Any] = ["colorName": "blue", "colorObject": UIColor.systemBlue]
    performSegue(withIdentifier: "showSecond", sender: sender)
  }
  
  @IBAction func greenTapped(_ sender: Any) {
    let sender: [String: Any] = ["colorName": "green", "colorObject": UIColor.systemGreen]
    performSegue(withIdentifier: "showSecond", sender: sender)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showSecond" {
      let secondView = segue.destination as! SecondViewController
      let object = sender as! [String: Any]
      secondView.colorObject = object["colorObject"] as? UIColor
      secondView.colorName = object["colorName"] as? String
    }
  }
}

