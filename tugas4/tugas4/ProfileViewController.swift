//
//  ProfileViewController.swift
//  tugas4
//
//  Created by Vincent Gunawan on 04/11/22.
//

import UIKit

class ProfileViewController: UIViewController {
  
  var username: String?
  
  @IBOutlet var profileImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var usernameLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setProfile()
    setUsername()
    setFriends()
    navigationItem.hidesBackButton = true
  }
  
  func setProfile() {
    profileImageView.layer.cornerRadius = profileImageView.frame.height/2
    profileImageView.layer.borderWidth = 4
    profileImageView.layer.borderColor = UIColor.white.cgColor
  }
  
  func setUsername() {
    nameLabel.text = username
    usernameLabel.text = "@\(username ?? "")"
  }
  
  func setFriends() {
 
  }
}
