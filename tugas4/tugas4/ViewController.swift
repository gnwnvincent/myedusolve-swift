//
//  ViewController.swift
//  tugas4
//
//  Created by Vincent Gunawan on 03/11/22.
//

import UIKit

class ViewController: UIViewController {
  
  @IBOutlet var usernameTextField: UITextField!
  @IBOutlet var passwordTextField: UITextField!
  @IBOutlet var loginButton: UIButton!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }
  
  @IBAction func loginTapped(_ sender: Any) {
    let username: String = usernameTextField.text ?? ""
    let password: String = passwordTextField.text ?? ""
    if (!username.isEmpty && !password.isEmpty) {
      loginButton.isEnabled = true
      performSegue(withIdentifier: "showProfile", sender: username)
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showProfile" {
      let vc = segue.destination as! ProfileViewController
      vc.username = sender as? String
    }
  }
}

extension UITextField{
  
  @IBInspectable var doneAccessory: Bool{
    get{
      return self.doneAccessory
    }
    set (hasDone) {
      if hasDone{
        addDoneButtonOnKeyboard()
      }
    }
  }
  
  func addDoneButtonOnKeyboard()
  {
    let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
    doneToolbar.barStyle = .default
    
    let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
    
    let items = [flexSpace, done]
    doneToolbar.items = items
    doneToolbar.sizeToFit()
    
    self.inputAccessoryView = doneToolbar
  }
  
  @objc func doneButtonAction()
  {
    self.resignFirstResponder()
  }
}
